#!/bin/bash
set -e
cd -- "$(dirname -- "${0}")/../../.."

if [ -z "${cc_path}" ]; then
	cc_path="cc"
fi

readonly system_name="$(uname -s)"

if [[ "${system_name}" != "MINGW"* ]] && [[ "${system_name}" != "CYGWIN"* ]]; then
	echo "This script is supposed to run on the Win32 platform !!!"
	exit 1
fi

readonly machine="$("${cc_path}" -dumpmachine)"

if [[ "${machine}" == *"-cygwin" ]]; then
	readonly use_flto=0
else
	readonly use_flto=1
fi

mk_slunk() {
	make -B CC="${cc_path}" MARCH=${2} MTUNE=${3} STATIC=1 STRIP=1 FLTO=${use_flto}
	cp -vf "frontend/bin/slunkcrypt" "out/slunkcrypt-${1}"
}

mkdir -p "out"

case "${machine}" in
	x86_64*)
		mk_slunk "x86_64" "x86-64" "nocona"
		;;
	i686*)
		mk_slunk "i686" "pentiumpro" "pentium3"
		;;
	*)
		echo "Unknown target CPU type !!!"
		exit 1
esac

if [[ "${machine}" == *"-cygwin" ]]; then
	cp -vfu "$(which cygwin1.dll)" "out"
fi

./etc/build/build_info.sh "${cc_path}" > "out/.build_info"

echo "Build completed successfully."
