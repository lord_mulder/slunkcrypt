#!/bin/sh
set -e
cd -- "$(dirname -- "${0}")/../../.."

if [ -z "${cc_path}" ]; then
	cc_path="/usr/local/bin/gcc14"
fi

mk_slunk() {
	gmake -B CC="${cc_path}" CPU=${1} MARCH=${3} MTUNE=${4} STATIC=1 STRIP=1
	cp -vf "frontend/bin/slunkcrypt" "out/slunkcrypt-${2}"
}

if [ "$(uname -s)" != "FreeBSD" ]; then
	echo "This script is supposed to run on the FreeBSD platform !!!"
	exit 1
fi

rm -rf "out" && mkdir -p "out"

mk_slunk 32 "i686"   "pentiumpro" "pentium3"
mk_slunk 64 "x86_64" "x86-64"     "nocona"

./etc/build/build_info.sh "${cc_path}" > "out/.build_info"

echo "Build completed successfully."
