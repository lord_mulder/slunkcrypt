# ---------------------------------------------------------------------------
# Options
# ---------------------------------------------------------------------------

DEBUG  ?= 0
NALYZE ?= 0
ASAN   ?= 0
STATIC ?= 0
SHARED ?= 0
FLTO   ?= 0
FPGO   ?= 0
STRIP  ?= 0
CPU    ?= 0
THREAD ?= 1

$(info Options: DEBUG=$(DEBUG), NALYZE=$(NALYZE), ASAN=$(ASAN), STATIC=$(STATIC), SHARED=$(SHARED), FLTO=$(FLTO), FPGO=$(FPGO), STRIP=$(STRIP), CPU=$(CPU), THREAD=$(THREAD))

ifneq ($(SHARED),0)
  ifneq ($(STATIC),0)
    $(error Options SHARED=1 and STATIC=1 are mutually exclusive!)
  endif
endif

# ---------------------------------------------------------------------------
# Directories
# ---------------------------------------------------------------------------

SUBDIR_APP := frontend
SUBDIR_LIB := libslunkcrypt

# ---------------------------------------------------------------------------
# Flags
# ---------------------------------------------------------------------------

CFLAGS = -I$(SUBDIR_LIB)/include -std=gnu99 -Wall -pedantic

ifneq (,$(firstword $(filter 32 64,$(CPU))))
  CFLAGS += -m$(firstword $(CPU))
endif
ifneq (,$(firstword $(TARGET)))
  CFLAGS += --target=$(firstword $(TARGET))
  LDFLGS += --target=$(firstword $(TARGET))
endif

ifneq (,$(firstword $(MARCH)))
  CFLAGS += -march=$(firstword $(MARCH))
endif
ifneq (,$(firstword $(MTUNE)))
  CFLAGS += -mtune=$(firstword $(MTUNE))
endif

ifneq ($(ASAN),0)
  CONFIG := _a
  CFLAGS += -O1 -g -fsanitize=address -fno-omit-frame-pointer -fno-optimize-sibling-calls
else
  ifneq ($(DEBUG),0)
    CONFIG := _g
    CFLAGS += -Og -g
  else
    CFLAGS += -O3 -DNDEBUG
    ifneq ($(FLTO),0)
      CFLAGS += -flto
    endif
    ifneq ($(FPGO),0)
      CFLAGS += -fprofile-$(firstword $(FPGO))
    endif
  endif
endif
ifneq ($(NALYZE),0)
  CFLAGS += -fanalyzer
endif

MACHINE := $(strip $(shell $(CC) -dumpmachine))
ifeq (,$(MACHINE))
  $(error Failed to determine target machine, please check CC is working!)
else
  $(info Target machine type: $(MACHINE))
endif

ifneq (,$(firstword $(filter %mingw32 %-windows-gnu %-cygwin %-cygnus,$(MACHINE))))
  EXE_SUFFIX := .exe
  LIB_SUFFIX := .$(if $(subst 0,,$(SHARED)),dll,a)
else
  LIB_SUFFIX := .$(if $(subst 0,,$(SHARED)),$(if $(findstring -apple-darwin,$(MACHINE)),dylib,so),a)
endif

ifneq (,$(firstword $(filter %-w64-mingw32 %w64-windows-gnu,$(MACHINE))))
  LDFLGS += -mconsole -municode
endif

ifeq ($(STATIC),1)
  LDFLGS += -static
endif

ifneq ($(STRIP),0)
  LDFLGS += -s
endif

ifeq ($(THREAD),1)
  LDFLGS += -lpthread
else
  CFLAGS += -DSLUNKBUILD_NOTHREADS
endif

ifneq (,$(firstword $(filter %-pc-haiku %-unknown-haiku,$(MACHINE))))
  LDFLGS += -lbsd
endif

APP_CFLAGS = $(CFLAGS)
APP_LDFLGS = -L$(SUBDIR_LIB)/lib -l$(VERSION_LIB) $(LDFLGS)
LIB_CFLAGS = $(CFLAGS)
LIB_LDFLGS = $(LDFLGS)

ifneq ($(SHARED),0)
  LIB_CFLAGS += -fPIC
  ifneq (,$(firstword $(filter %-w64-mingw32 %w64-windows-gnu,$(MACHINE))))
    LIB_LDFLGS = -Wl,--out-implib,"$@.a" $(LDFLGS)
  endif
endif

# ---------------------------------------------------------------------------
# File names
# ---------------------------------------------------------------------------

VERSION_LIB := slunkcrypt$(CONFIG)-1

OUTNAME_APP := slunkcrypt$(CONFIG)$(EXE_SUFFIX)
OUTNAME_LIB := lib$(VERSION_LIB)$(LIB_SUFFIX)

OUTPATH_APP := $(SUBDIR_APP)/bin/$(OUTNAME_APP)
OUTPATH_LIB := $(SUBDIR_LIB)/lib/$(OUTNAME_LIB)

SOURCES_APP := $(wildcard $(SUBDIR_APP)/src/*.c)
OBJECTS_APP := $(patsubst $(SUBDIR_APP)/src/%.c,$(SUBDIR_APP)/obj/%$(CONFIG).o,$(SOURCES_APP))

SOURCES_LIB := $(wildcard $(SUBDIR_LIB)/src/*.c)
OBJECTS_LIB := $(patsubst $(SUBDIR_LIB)/src/%.c,$(SUBDIR_LIB)/obj/%$(CONFIG).o,$(SOURCES_LIB))

ifneq ($(filter %-mingw32 %-windows-gnu %-cygwin %-cygnus,$(MACHINE)),)
  RCFILES_APP := $(wildcard $(SUBDIR_APP)/res/*.rc)
  OBJECTS_APP += $(patsubst $(SUBDIR_APP)/res/%.rc,$(SUBDIR_APP)/obj/%.rsrc.o,$(RCFILES_APP))
endif

# ---------------------------------------------------------------------------
# Targets
# ---------------------------------------------------------------------------

.PHONY: all build clean

all: clean build

build: $(OUTPATH_APP)

$(OUTPATH_APP): $(OBJECTS_APP) $(OUTPATH_LIB)
	@mkdir -p $(@D)
	$(CC) $(APP_CFLAGS) $(OBJECTS_APP) -o $@ $(APP_LDFLGS)

$(OUTPATH_LIB): $(OBJECTS_LIB)
	@mkdir -p $(@D)
ifneq ($(SHARED),0)
	$(CC) $(LIB_CFLAGS) $^ -shared -o $@ $(LIB_LDFLGS)
else
	$(AR) rcs $@ $^
endif

$(SUBDIR_APP)/obj/%$(CONFIG).o: $(SUBDIR_APP)/src/%.c
	@mkdir -p $(@D)
	$(CC) $(APP_CFLAGS) -c $< -o $@

$(SUBDIR_APP)/obj/%.rsrc.o: $(SUBDIR_APP)/res/%.rc
	@mkdir -p $(@D)
	windres -o $@ $<

$(SUBDIR_LIB)/obj/%$(CONFIG).o: $(SUBDIR_LIB)/src/%.c
	@mkdir -p $(@D)
	$(CC) $(LIB_CFLAGS) -c $< -o $@

clean:
	$(RM) $(SUBDIR_APP)/obj/* $(SUBDIR_APP)/lib/* $(SUBDIR_APP)/bin/*
	$(RM) $(SUBDIR_LIB)/obj/* $(SUBDIR_LIB)/lib/* $(SUBDIR_LIB)/bin/*
