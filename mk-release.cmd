@echo off
setlocal enabledelayedexpansion

set ECHO="%~dp0.\etc\build\win32\cecho.exe"
if "%MSVC_PATH%"=="" (
	set "MSVC_PATH=C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC"
)

if not exist "%MSVC_PATH%\Auxiliary\Build\vcvarsall.bat" (
	%ECHO% red "\nMSVC not found. Please check MSVC_PATH and try again^!\n"
	pause
	goto:eof
)

for %%p in (x86,x64,ARM64) do (
	call "%MSVC_PATH%\Auxiliary\Build\vcvarsall.bat" %%p
	for %%c in (Release,Release_SSE2,Release_DLL,Debug) do (
		if not "%%p::%%c"=="ARM64::Release_SSE2" (
			%ECHO% white "\n------------------------------------------------------------------------------"
			%ECHO% white "Clean [%%p:%%c]"
			%ECHO% white "------------------------------------------------------------------------------\n"
			MSBuild.exe /property:Configuration=%%c /property:Platform=%%p /target:Clean /verbosity:normal "%~dp0\Slunk.sln"
			if not "!ERRORLEVEL!"=="0" goto:BuildFailed
			%ECHO% white "\n------------------------------------------------------------------------------"
			%ECHO% white "Compile [%%p:%%c]"
			%ECHO% white "------------------------------------------------------------------------------\n"
			MSBuild.exe /property:Configuration=%%c /property:Platform=%%p /target:Build /verbosity:normal "%~dp0\Slunk.sln"
			if not "!ERRORLEVEL!"=="0" goto:BuildFailed
		)
	)
)

%ECHO% green "\nBuild completed successfully.\n"
pause
goto:eof

:BuildFailed
%ECHO% red "\nBuild has failed ^!^!^!\n"
pause
