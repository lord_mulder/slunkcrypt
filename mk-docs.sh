#!/bin/bash
set -e
cd -- "$(dirname -- "${BASH_SOURCE[0]}")"

pandoc -o README.html --standalone --embed-resources --toc --toc-depth=3 --css etc/style/gh-pandoc.min.css README.md
