﻿/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace com.muldersoft.slunkcrypt.gui.ctrls
{
    public partial class Hyperlink : UserControl
    {
        public event MouseButtonEventHandler Click;

        public Hyperlink()
        {
            InitializeComponent();
        }

        public string LinkText
        {
            get
            {
                return Label_Hyperlink.Text;
            }
            set
            {
                Label_Hyperlink.Text = value;
            }
        }

        public FontFamily LinkFontFamily
        {
            get
            {
                return Label_Hyperlink.FontFamily;
            }
            set
            {
                Label_Hyperlink.FontFamily = value;
            }
        }

        public double LinkFontSize
        {
            get
            {
                return Label_Hyperlink.FontSize;
            }
            set
            {
                Label_Hyperlink.FontSize = value;
            }
        }

        private void Label_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIElement inputElement;
            if (!ReferenceEquals(inputElement = sender as UIElement, null))
            {
                inputElement.CaptureMouse();
            }
        }

        private void Label_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            UIElement inputElement;
            if (!ReferenceEquals(inputElement = sender as UIElement, null))
            {
                if (inputElement.IsMouseCaptured)
                {
                    inputElement.ReleaseMouseCapture();
                    if (!ReferenceEquals(VisualTreeHelper.HitTest(inputElement, e.GetPosition(inputElement)), null))
                    {
                        Click?.Invoke(this, e);
                    }
                }
            }
        }
    }
}
