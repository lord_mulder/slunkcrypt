﻿/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

using System;
using System.ComponentModel;
using System.Reflection;

namespace com.muldersoft.slunkcrypt.gui.utils
{
    public static class EnumHelper
    {
        public static bool IsDefined<T>(T value) where T : struct, IConvertible
        {
            try
            {
                return Enum.IsDefined(typeof(T), value);
            }
            catch
            {
                return false;
            }
        }

        public static string GetDescription(this Enum value)
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            if (!string.IsNullOrEmpty(name))
            {
                FieldInfo field = type.GetField(name);
                if (!ReferenceEquals(field, null))
                {
                    DescriptionAttribute attr = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
                    if (!ReferenceEquals(attr, null))
                    {
                        return attr.Description;
                    }
                }
                return name;
            }
            return null;
        }
    }
}
