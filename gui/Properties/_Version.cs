﻿/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

namespace com.muldersoft.slunkcrypt.gui.Properties
{
    internal static class _Version
    {
        public const string VERS_MAJOR = "1";
        public const string VERS_MINOR = "3";
        public const string VERS_PATCH = "2";
    }
}
