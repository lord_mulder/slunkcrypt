/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

#ifndef INC_SLUNKAPP_WORSTPWD_H
#define INC_SLUNKAPP_WORSTPWD_H

#include "platform.h"

int is_passphrase_blacklisted(const char *const passwd);

#endif
