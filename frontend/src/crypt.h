/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

#ifndef INC_SLUNKAPP_CRYPT_H
#define INC_SLUNKAPP_CRYPT_H

#include "platform.h"

typedef struct
{
	int keep_incomplete;
	int legacy_compat;
	int debug_logging;
	size_t thread_count;
}
crypt_options_t;

int encrypt(const char *const passphrase, const CHR *const input_path, const CHR *const output_path, const crypt_options_t *const options);
int decrypt(const char *const passphrase, const CHR *const input_path, const CHR *const output_path, const crypt_options_t *const options);

#endif
