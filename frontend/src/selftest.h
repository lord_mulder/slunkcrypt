/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

#ifndef INC_SLUNKAPP_SELFTEST_H
#define INC_SLUNKAPP_SELFTEST_H

#include "platform.h"

int selftest_routine(const size_t thread_count, const int debug);

#endif
