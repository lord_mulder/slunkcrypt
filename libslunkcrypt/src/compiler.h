/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

#ifndef INC_SLUNKCRYPT_COMPILER_H
#define INC_SLUNKCRYPT_COMPILER_H

#ifdef _WIN32
#  define WIN32_LEAN_AND_MEAN 1
#  define _CRT_SECURE_NO_WARNINGS 1
#else
#  define _GNU_SOURCE 1
#endif

/* Intel(R) oneAPI DPC++/C++ Compiler */
#if defined(__INTEL_LLVM_COMPILER) && (!defined(__GNUC__))
#  define __GNUC__ 4
#endif

/* Compiler compatibility */
#if defined(_MSC_VER) && (!defined(__GNUC__))
#  define INLINE __inline
#  define UNUSED __pragma(warning(suppress: 4189))
#elif defined(__GNUC__)
#  define INLINE __inline__
#  define UNUSED __attribute__((unused))
#else
#  define INLINE inline
#  define UNUSED
#endif

#endif
