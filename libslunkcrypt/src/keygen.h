/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

#ifndef INC_SLUNKCRYPT_KEYGEN_H
#define INC_SLUNKCRYPT_KEYGEN_H

#include <stdlib.h>
#include <stdint.h>

typedef struct
{
	uint64_t a, b, c;
}
keydata_t;

void slunkcrypt_keygen(keydata_t *const key, const uint64_t salt, const uint16_t pepper, const uint8_t* const passwd, const size_t passwd_len, const size_t rounds);

#endif
