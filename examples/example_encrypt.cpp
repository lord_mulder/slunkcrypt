/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

//SlunkCrypt API
#include <slunkcrypt.hpp>

//CRT
#include <cstdlib>
#include <iostream>
#include <fstream>

//Example password
static std::string EXAMPLE_PASSWORD("cMRe5E5D)'!2?5]QDlCQ4tBb");

//Const
#define BUFF_SIZE 4096U

static int encrypt_main(int argc, char *argv[])
{
	std::cerr << "SlunkCrypt encrypt sample [" << __DATE__ << "]" << std::endl;
	std::cerr << "using libSlunkCrypt v" << SLUNKCRYPT_VERSION_MAJOR << '.' << SLUNKCRYPT_VERSION_MINOR << '.' << SLUNKCRYPT_VERSION_PATCH << '\n' << std::endl;

	if (argc < 3)
	{
		std::cerr << "Usage:\n  encrypt.exe <plaintext.txt> <ciphertext.enc>\n" << std::endl;
		return -1;
	}

	// -----------------------------------------------------------
	// Open input/output files
	// -----------------------------------------------------------

	std::ifstream file_src(argv[1], std::ios::binary);
	if (!file_src.is_open())
	{
		std::cerr << "Error: Failed to open input file for reading!" << std::endl;
		return -1;
	}

	std::ofstream file_dst(argv[2], std::ios::binary);
	if (!file_dst.is_open())
	{
		std::cerr << "Error: Failed to open output file for writing!" << std::endl;
		return -1;
	}

	// -----------------------------------------------------------
	// Initialize the SlunkCryptEncr instance
	// -----------------------------------------------------------

	std::cerr << "Initializing key, please wait... " << std::flush;

	uint8_t buffer[BUFF_SIZE];
	slunkcrypt::Encryptor slunk_encrypt(EXAMPLE_PASSWORD);

	std::cerr << "done.\nSlunk-encrypting the file contents, please wait... " << std::flush;

	// -----------------------------------------------------------
	// Encryption loop
	// -----------------------------------------------------------

	file_src.exceptions(std::ifstream::badbit);
	file_dst.exceptions(std::ifstream::failbit | std::ifstream::badbit);

	try
	{
		while (file_src.good())
		{
			file_src.read(reinterpret_cast<char*>(buffer), BUFF_SIZE);
			const std::streamsize count = file_src.gcount();
			if (count > 0)
			{
				if (!slunk_encrypt.inplace(buffer, (size_t)count))
				{
					std::cerr << "failed!\n\nError: SlunkCrypt encryption has failed!" << std::endl;
					return -1;
				}
				file_dst.write(reinterpret_cast<char*>(buffer), count);
			}
		}
	}
	catch (std::ios_base::failure e)
	{
		std::cerr << "failed!\n\nI/O Error: \"" << e.code().message() << "\" [Code: " << e.code().value() << "]\n" << std::endl;
		return -1;
	}

	// -----------------------------------------------------------
	// Clean up
	// -----------------------------------------------------------

	std::cerr << "done.\n\nNonce: " << std::hex << std::uppercase << slunk_encrypt.get_nonce() << '\n' << std::endl;

	file_src.close();
	file_dst.close();

	return 0;
}

int main(int argc, char *argv[])
{
	try
	{
		return encrypt_main(argc, argv);
	}
	catch (std::exception e)
	{
		std::cerr << "\n\nException: \"" << e.what() << "\"\n" << std::endl;
		return -1;
	}
}
