/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

use std::env;
use slunkcrypt_rs::{get_version, get_build, generate_nonce};

const DEFAULT_TEST_LOOPS: usize = 8;

#[test]
fn test_get_version() {
    let (major, minor, patch) = get_version();
    assert!(major > 0);
    println!("Version: {}.{}.{}", major, minor, patch);
}

#[test]
fn test_get_build() {
    let build = get_build();
    println!("Build: \"{}\"", build);
}

#[test]
fn test_init_generate_nonce() {
    run_test(|| {
        let nonce = generate_nonce().expect("Failed to generate nonce!");
        println!("Nonce: {:16X}", nonce);
    });
}

fn run_test<F>(func: F) where F: Fn() {
    let loops = env::var("TEST_LOOPS").map_or(DEFAULT_TEST_LOOPS, |value| value.parse::<usize>().unwrap());
    for _ in 0..loops {
         func();
    }
}
