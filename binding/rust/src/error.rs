/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

use std::os::raw::c_int;
use crate::ffi::libslunkcrypt::{SLUNKCRYPT_ABORTED, SLUNKCRYPT_FAILURE, SLUNKCRYPT_SUCCESS};

/// SlunkCrypt error type.
///
/// This struct provides information about the SlunkCrypt error that has occurred.
#[derive(Debug)]
pub struct SlunkCryptError {
    kind: SlunkCryptErrorKind,
    description: &'static str
}

/// SlunkCrypt error kind enumeration.
/// 
/// This enumeration defines the *kind* of the SlunkCrypt error that has occurred.
#[derive(Clone, Copy, Debug)]
pub enum SlunkCryptErrorKind {
    /// The operation succeeded.
    Success,
    /// The operation was **not** executed because of invalid arguments.
    Invalid,
    /// The operation has failed.
    Failure,
    /// The operation was aborted by the user.
    Aborted,
    /// Unknown error. This is **not** supposed to happen.
    Unknown
}

impl SlunkCryptError {
    /// Creates a new error from the given kind and description.
    pub(crate) fn new(kind: SlunkCryptErrorKind, description: &'static str) -> Self {
        Self { kind, description }
    }

    /// Creates a new error from the given error code and description.
    pub(crate) fn from_retval(retval: c_int, description: &'static str) -> Self {
        let kind = match retval {
            SLUNKCRYPT_SUCCESS => SlunkCryptErrorKind::Success,
            SLUNKCRYPT_ABORTED => SlunkCryptErrorKind::Aborted,
            SLUNKCRYPT_FAILURE => SlunkCryptErrorKind::Failure,
            _                  => SlunkCryptErrorKind::Unknown
        };
        Self::new(kind, description)
    }

    /// Returns the [*kind*](SlunkCryptErrorKind) of the SlunkCrypt error that has occurred.
    pub fn kind(&self) -> SlunkCryptErrorKind {
        self.kind
    }

    /// Returns a detailed textual description of the SlunkCrypt error that has occurred.
    pub fn description(&self) -> &'static str {
        &self.description
    }
}
