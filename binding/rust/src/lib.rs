/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

//! # SlunkCrypt Rust Wrapper
//! This crate exposes the functionality of the [**SlunkCrypt**](https://gitlab.com/lord_mulder/slunkcrypt) library to to Rust developers.
//! 
//! Please see the [`SlunkCrypt`] struct for details!
//! 
//! ## Build Instructions
//! 
//! First the "native" SlunkCrypt library needs to be built, if not done yet:
//! ```sh
//! $ cd /home/JohnnyBeSlunk/dev/SlunkCrypt
//! $ make -B SHARED=1
//! ```
//! 
//! Now build the SlunkCrypt Rust wrapper crate:
//! ```sh
//! $ cd binding/rust
//! $ cargo build --release
//! ```
//!
//! ## Unit Tests 
//!
//! In order to run the unit tests, please type:
//!
//! ```sh
//! $ export LD_LIBRARY_PATH=/home/JohnnyBeSlunk/dev/SlunkCryptlibslunkcrypt/lib
//! $cargo test --release
//! ```

mod context;
mod error;
mod ffi;
mod functions;
mod passwd;

pub use context::SlunkCrypt;
pub use error::{SlunkCryptError, SlunkCryptErrorKind};
pub use functions::{generate_nonce, get_version, get_build};
pub use passwd::SlunkCryptPasswd;
