/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

package com.muldersoft.slunkcrypt.internal;

import com.muldersoft.slunkcrypt.internal.types.Context;
import com.muldersoft.slunkcrypt.internal.types.SizeT;
import com.muldersoft.slunkcrypt.internal.types.UInt64;
import com.muldersoft.slunkcrypt.internal.types.UInt64ByReference;
import com.muldersoft.slunkcrypt.internal.utilities.Constant;
import com.muldersoft.slunkcrypt.internal.utilities.Constant.StrPtrConst;
import com.muldersoft.slunkcrypt.internal.utilities.Constant.UInt16Const;
import com.sun.jna.Library;
import com.sun.jna.Native;

public interface SlunkCryptLibrary extends Library {

	static final SlunkCryptLibrary INSTANCE = Native.load("slunkcrypt-1", SlunkCryptLibrary.class);

	static class Version {
		public static final UInt16Const MAJOR = Constant.ofUInt16(INSTANCE, "SLUNKCRYPT_VERSION_MAJOR");
		public static final UInt16Const MINOR = Constant.ofUInt16(INSTANCE, "SLUNKCRYPT_VERSION_MINOR");
		public static final UInt16Const PATCH = Constant.ofUInt16(INSTANCE, "SLUNKCRYPT_VERSION_PATCH");
		public static final StrPtrConst BUILD = Constant.ofStrPtr(INSTANCE, "SLUNKCRYPT_BUILD");
	}

	Context slunkcrypt_alloc_ext(UInt64 nonce, byte[] passwd, SizeT passwd_len, int mode, SlunkCryptParam param);
	int slunkcrypt_reset(Context context, UInt64 nonce, byte[] passwd, SizeT passwd_len, int mode);
	void slunkcrypt_free(Context context);
	int slunkcrypt_inplace(Context context, byte[] buffer, SizeT data_len);
	int slunkcrypt_process(Context context, byte[] input, byte[] output, SizeT data_len);
	int slunkcrypt_generate_nonce(UInt64ByReference nonce);
}
