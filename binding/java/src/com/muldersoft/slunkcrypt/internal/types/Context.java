/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

package com.muldersoft.slunkcrypt.internal.types;

import com.sun.jna.IntegerType;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

public class Context extends IntegerType {

	private static final long serialVersionUID = 1L;

	public static final Context NULL = new Context();

	public Context() {
		super(Native.POINTER_SIZE, Pointer.nativeValue(Pointer.NULL), true);
	}

	public Context(final long value) {
		super(Native.POINTER_SIZE, value, true);
	}

	public Pointer toPointer() {
		return Pointer.createConstant(longValue());
	}
}
