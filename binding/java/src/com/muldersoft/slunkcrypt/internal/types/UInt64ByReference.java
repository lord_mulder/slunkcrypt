/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

package com.muldersoft.slunkcrypt.internal.types;

import com.sun.jna.ptr.ByReference;

public class UInt64ByReference extends ByReference {

	public UInt64ByReference() {
		super(Long.BYTES);
	}
	
	private UInt64ByReference(final long value) {
		super(Long.BYTES);
		setValue(value);
	}

	public void setValue(final long value) {
		getPointer().setLong(0, value);
	}
	
	public long getValue() {
		return getPointer().getLong(0);
	}

	public static UInt64ByReference of(final long value) {
		return new UInt64ByReference(value);
	}
}
