package com.muldersoft.slunkcrypt.internal;

import com.muldersoft.slunkcrypt.internal.types.SizeT;
import com.muldersoft.slunkcrypt.internal.types.UInt16;
import com.sun.jna.Structure;
import com.sun.jna.Structure.FieldOrder;

@FieldOrder({ "version", "thread_count", "legacy_compat", "debug_logging" })
public class SlunkCryptParam extends Structure {
	
	public static final UInt16 SLUNK_PARAM_VERSION = UInt16.of((short)2);

	public SlunkCryptParam() {
		version = SLUNK_PARAM_VERSION;
	}

	public UInt16 version;
	public SizeT thread_count;
	public int legacy_compat;
	public int debug_logging;
}
