/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

package com.muldersoft.slunkcrypt;

import java.io.IOException;
import java.util.Objects;

/**
 * Signals that a SlunkCrypt operation has failed or that it has been aborted by the user.
 * <p>Details are provided via {@link ErrorKind}.</p>
 */
public final class SlunkCryptException extends IOException {

	/**
	 * The kind of error that has triggered a {@link SlunkCryptException}.
	 */
	public enum ErrorKind {
		/** The operation completed successfully. */
		Success,
		/** The operation has failed. */
		Failure,
		/** The operation was aborted by the user. */
		Aborted,
		/** An unknown kind of error has occurred. */
		Unknown
	}

	private static final long serialVersionUID = 1L;
	
	private final ErrorKind kind;

	private static final int SLUNKCRYPT_SUCCESS =  0;
	private static final int SLUNKCRYPT_FAILURE = -1;
	private static final int SLUNKCRYPT_ABORTED = -2;

	SlunkCryptException(final ErrorKind kind, final String message) {
		super(message);
		this.kind = Objects.requireNonNull(kind);
	}

	static boolean isSuccess(final int errorCode) {
		return errorCode == SLUNKCRYPT_SUCCESS;
	}

	static SlunkCryptException mapErrorCode(final int errorCode, final String message) {
		switch(errorCode) {
			case SLUNKCRYPT_SUCCESS:
				return new SlunkCryptException(ErrorKind.Success, message);
			case SLUNKCRYPT_FAILURE:
				return new SlunkCryptException(ErrorKind.Failure, message);
			case SLUNKCRYPT_ABORTED:
				return new SlunkCryptException(ErrorKind.Aborted, message);
			default:
				return new SlunkCryptException(ErrorKind.Unknown, message);
		}
	}

	/**
	 * Get the kind of error that has triggered this exception.
	 * @return the {@link com.muldersoft.slunkcrypt.SlunkCryptException.ErrorKind ErrorKind}
	 */
	public ErrorKind getErrorKind() {
		return kind;
	}
}
